function p = pathdef
%PATHDEF Search path defaults.
%   PATHDEF returns a string that can be used as input to MATLABPATH
%   in order to set the path.

  
%   Copyright 1984-2016 The MathWorks, Inc.


% DO NOT MODIFY THIS FILE.  IT IS AN AUTOGENERATED FILE.  
% EDITING MAY CAUSE THE FILE TO BECOME UNREADABLE TO 
% THE PATHTOOL AND THE INSTALLER.

p = [...
%%% BEGIN ENTRIES %%%
     '/extra/krishna/ksvd:', ...
     '/extra/krishna/ompbox10:', ...
     matlabroot,'/toolbox/matlab/elfun:', ...
     matlabroot,'/toolbox/matlab/timefun:', ...
     matlabroot,'/toolbox/matlab/matfun:', ...
     matlabroot,'/toolbox/matlab/general:', ...
     matlabroot,'/toolbox/matlab/datafun:', ...
     matlabroot,'/toolbox/matlab/lang:', ...
     matlabroot,'/toolbox/matlab/mvm:', ...
     matlabroot,'/toolbox/matlab/sparfun:', ...
     matlabroot,'/toolbox/matlab/ops:', ...
     matlabroot,'/toolbox/matlab/funfun:', ...
     matlabroot,'/toolbox/matlab/randfun:', ...
     matlabroot,'/toolbox/matlab/specfun:', ...
     matlabroot,'/toolbox/matlab/iofun:', ...
     matlabroot,'/toolbox/matlab/polyfun:', ...
     matlabroot,'/toolbox/matlab/elmat:', ...
     matlabroot,'/toolbox/matlab/datatypes:', ...
     matlabroot,'/toolbox/matlab/strfun:', ...
     matlabroot,'/toolbox/matlab/winfun:', ...
     matlabroot,'/toolbox/matlab/winfun/NET:', ...
     matlabroot,'/toolbox/matlab/icons:', ...
     matlabroot,'/toolbox/local:', ...
     matlabroot,'/toolbox/matlab/testframework/ext:', ...
     matlabroot,'/toolbox/matlab/graph2d:', ...
     matlabroot,'/toolbox/matlab/graph3d:', ...
     matlabroot,'/toolbox/matlab/graphics:', ...
     matlabroot,'/toolbox/matlab/graphics/obsolete:', ...
     matlabroot,'/toolbox/matlab/plottools:', ...
     matlabroot,'/toolbox/matlab/scribe:', ...
     matlabroot,'/toolbox/matlab/scribe/obsolete:', ...
     matlabroot,'/toolbox/matlab/specgraph:', ...
     matlabroot,'/toolbox/matlab/uitools:', ...
     matlabroot,'/toolbox/matlab/uitools/obsolete:', ...
     matlabroot,'/toolbox/matlab/depfun:', ...
     matlabroot,'/toolbox/matlab/bigdata:', ...
     matlabroot,'/toolbox/matlab/codetools:', ...
     matlabroot,'/toolbox/matlab/codetools/embeddedoutputs:', ...
     matlabroot,'/toolbox/matlab/guide:', ...
     matlabroot,'/toolbox/matlab/hardware/stubs:', ...
     matlabroot,'/toolbox/matlab/datamanager:', ...
     matlabroot,'/toolbox/matlab/testframework/performance:', ...
     matlabroot,'/toolbox/matlab/datastoreio:', ...
     matlabroot,'/toolbox/matlab/graphfun:', ...
     matlabroot,'/toolbox/matlab/demos:', ...
     matlabroot,'/toolbox/matlab/mapreduceio:', ...
     matlabroot,'/toolbox/matlab/findfiles:', ...
     matlabroot,'/toolbox/matlab/testframework/core:', ...
     matlabroot,'/toolbox/matlab/testframework/obsolete:', ...
     matlabroot,'/toolbox/matlab/optimfun:', ...
     matlabroot,'/toolbox/matlab/verctrl:', ...
     matlabroot,'/toolbox/matlab/images:', ...
     matlabroot,'/toolbox/matlab/testframework/measurement:', ...
     matlabroot,'/toolbox/matlab/helptools:', ...
     matlabroot,'/toolbox/matlab/testframework/parallel:', ...
     matlabroot,'/toolbox/simulink/sldependency:', ...
     matlabroot,'/toolbox/simulink/ui/library_browser/core/m:', ...
     matlabroot,'/toolbox/simulink/simdemos/automotive:', ...
     matlabroot,'/toolbox/simulink/simdemos/automotive/powerwindow:', ...
     matlabroot,'/toolbox/simulink/simdemos/simgeneral:', ...
     matlabroot,'/toolbox/simulink/simulink/templates/product:', ...
     matlabroot,'/toolbox/simulink/simdemos/aerospace:', ...
     matlabroot,'/toolbox/simulink/sysarch/sysarch:', ...
     matlabroot,'/toolbox/rtw/accel:', ...
     matlabroot,'/toolbox/coder/simulinkcoder_core:', ...
     matlabroot,'/toolbox/simulink/blocks/library:', ...
     matlabroot,'/toolbox/simulink/blocks/library/simulinkcoder:', ...
     matlabroot,'/toolbox/simulink/blocks/obsolete:', ...
     matlabroot,'/toolbox/simulink/simdemos/simfeatures:', ...
     matlabroot,'/toolbox/simulink/simdemos/simfeatures/modelreference:', ...
     matlabroot,'/toolbox/simulink/simdemos/simfeatures/datadictionary:', ...
     matlabroot,'/toolbox/simulinktest/core/testsequence/testsequence:', ...
     matlabroot,'/toolbox/simulink/simulink/templates/core:', ...
     matlabroot,'/toolbox/simulink/simulink/modeladvisor:', ...
     matlabroot,'/toolbox/simulink/simulink/modeladvisor/fixpt:', ...
     matlabroot,'/toolbox/simulink/simulink/modeladvisor/misra:', ...
     matlabroot,'/toolbox/simulink/sdi:', ...
     matlabroot,'/toolbox/coder/objectives:', ...
     matlabroot,'/toolbox/simulink/components:', ...
     matlabroot,'/toolbox/simulink/dee:', ...
     matlabroot,'/toolbox/simulink/simdemos/automotive/fuelsys:', ...
     matlabroot,'/toolbox/simulink/simulink/frameedit:', ...
     matlabroot,'/toolbox/simulink/blocks:', ...
     matlabroot,'/toolbox/simulink/simulink/dataclasses:', ...
     matlabroot,'/toolbox/simulink/simulink:', ...
     matlabroot,'/toolbox/simulink/simulink/MPlayIO:', ...
     matlabroot,'/toolbox/simulink/simulink/dataobjectwizard:', ...
     matlabroot,'/toolbox/simulink/simulink/slresolve:', ...
     matlabroot,'/toolbox/simulink/simulink/units:', ...
     matlabroot,'/toolbox/simulink/simulink/resources:', ...
     matlabroot,'/toolbox/simulink/simulink/model_transformer:', ...
     matlabroot,'/toolbox/simulinktest/core/simharness/simharness:', ...
     matlabroot,'/toolbox/simulink/simdemos/industrial:', ...
     matlabroot,'/toolbox/simulink/simdemos/dataclasses:', ...
     matlabroot,'/toolbox/simulink/simdemos:', ...
     matlabroot,'/toolbox/simulink/hmi:', ...
     matlabroot,'/toolbox/stateflow/stateflow:', ...
     matlabroot,'/toolbox/stateflow/coder:', ...
     matlabroot,'/toolbox/stateflow/sfdemos:', ...
     matlabroot,'/toolbox/stateflow/sftemplates:', ...
     matlabroot,'/toolbox/shared/imageio:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/python:', ...
     matlabroot,'/toolbox/shared/supportsoftware/launcher:', ...
     matlabroot,'/toolbox/shared/supportsoftware/services:', ...
     matlabroot,'/toolbox/shared/controllib/graphics:', ...
     matlabroot,'/toolbox/shared/controllib/graphics/utils:', ...
     matlabroot,'/toolbox/shared/controllib/graphics/plotoptions:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/json:', ...
     matlabroot,'/toolbox/shared/hotpluglib:', ...
     matlabroot,'/toolbox/matlab/connector/connector:', ...
     matlabroot,'/toolbox/matlab/supportpackagemanagement:', ...
     matlabroot,'/toolbox/shared/asynciolib:', ...
     matlabroot,'/toolbox/hdlcoder/hdllib/ml_lib:', ...
     matlabroot,'/toolbox/matlab/addons_toolbox:', ...
     matlabroot,'/toolbox/shared/rptgen:', ...
     matlabroot,'/toolbox/shared/comparisons:', ...
     matlabroot,'/toolbox/shared/multimedia:', ...
     matlabroot,'/toolbox/shared/cmlink/api:', ...
     matlabroot,'/toolbox/matlab/networklib:', ...
     matlabroot,'/toolbox/matlab/addons:', ...
     matlabroot,'/toolbox/matlab/addons/cef:', ...
     matlabroot,'/toolbox/matlab/addons/fallbackmanager:', ...
     matlabroot,'/toolbox/matlab/addons/supportpackages:', ...
     matlabroot,'/toolbox/matlab/cefclient:', ...
     matlabroot,'/toolbox/matlab/datatools/inspector/matlab:', ...
     matlabroot,'/toolbox/matlab/toolboxmanagement/matlab_api:', ...
     matlabroot,'/toolbox/matlab/spf/matlabhost:', ...
     matlabroot,'/toolbox/matlab/toolstrip:', ...
     matlabroot,'/toolbox/shared/io:', ...
     matlabroot,'/toolbox/matlab/addons_product:', ...
     matlabroot,'/toolbox/matlab/apps:', ...
     matlabroot,'/toolbox/shared/hadoopserializer:', ...
     matlabroot,'/toolbox/shared/mlreportgen/ppt:', ...
     matlabroot,'/toolbox/shared/mlreportgen/ppt/ppt:', ...
     matlabroot,'/toolbox/shared/mlreportgen/ppt/ppt/help:', ...
     matlabroot,'/toolbox/shared/hwconnectinstaller:', ...
     matlabroot,'/toolbox/matlab/addons_zip:', ...
     matlabroot,'/toolbox/matlab/serial:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/webservices/wsdl:', ...
     matlabroot,'/toolbox/matlab/uicomponents/uicomponents:', ...
     matlabroot,'/toolbox/matlab/uicomponents/uicomponents/graphics:', ...
     matlabroot,'/toolbox/matlab/external/engines/engine_api:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/webservices/restful:', ...
     matlabroot,'/toolbox/matlab/external/interfaces:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/webservices:', ...
     matlabroot,'/toolbox/shared/diagnostic:', ...
     matlabroot,'/toolbox/shared/hwconnectinstaller/common:', ...
     matlabroot,'/toolbox/shared/mlreportgen/dom:', ...
     matlabroot,'/toolbox/shared/mlreportgen/dom/dom:', ...
     matlabroot,'/toolbox/shared/mlreportgen/dom/dom/help:', ...
     matlabroot,'/toolbox/shared/spreadsheet:', ...
     matlabroot,'/toolbox/matlab/system:', ...
     matlabroot,'/toolbox/matlab/configtools:', ...
     matlabroot,'/toolbox/shared/appdes/services:', ...
     matlabroot,'/toolbox/shared/advisor:', ...
     matlabroot,'/toolbox/shared/supportpkgservices/legacysupportpkginfo:', ...
     matlabroot,'/toolbox/shared/supportpkgservices/installservices:', ...
     matlabroot,'/toolbox/shared/instrument:', ...
     matlabroot,'/toolbox/shared/deviceplugindetection:', ...
     matlabroot,'/toolbox/matlab/appdesigner/appdesigner:', ...
     matlabroot,'/toolbox/matlab/uitools/uicomponents/components:', ...
     matlabroot,'/toolbox/shared/m3i:', ...
     matlabroot,'/toolbox/shared/testmeaslib/graphics:', ...
     matlabroot,'/toolbox/matlab/audiovideo:', ...
     matlabroot,'/toolbox/shared/dastudio:', ...
     matlabroot,'/toolbox/matlab/toolbox_packaging:', ...
     matlabroot,'/toolbox/shared/coder/coder:', ...
     matlabroot,'/toolbox/matlab/webcam:', ...
     matlabroot,'/toolbox/shared/testmeaslib/general:', ...
     matlabroot,'/toolbox/shared/networklib:', ...
     matlabroot,'/toolbox/simulink/simulink/slproject:', ...
     matlabroot,'/toolbox/simulink/simulink/slproject/menu:', ...
     matlabroot,'/toolbox/matlab/spf/matlabservices:', ...
     matlabroot,'/toolbox/matlab/timeseries:', ...
     matlabroot,'/toolbox/matlab/hds:', ...
     matlabroot,'/toolbox/matlab/system/editor:', ...
     matlabroot,'/toolbox/matlab/external/interfaces/webservices/http:', ...
     matlabroot,'/toolbox/shared/controllib/general:', ...
     matlabroot,'/toolbox/matlab/appdesigner/appdesigner/interface:', ...
     matlabroot,'/toolbox/matlab/imagesci:', ...
     matlabroot,'/toolbox/shared/simulink:', ...
     matlabroot,'/toolbox/shared/system/sfun:', ...
     matlabroot,'/toolbox/shared/cgxe/cgxe:', ...
     matlabroot,'/toolbox/simulink/simulink/upgradeadvisor:', ...
     matlabroot,'/toolbox/shared/dspblks/dspblks:', ...
     matlabroot,'/toolbox/shared/dspblks/dspmex:', ...
     matlabroot,'/toolbox/shared/dsp/simulink/dsp:', ...
     matlabroot,'/toolbox/fixpoint:', ...
     matlabroot,'/toolbox/fixpoint/fpca:', ...
     matlabroot,'/toolbox/shared/dastudio/dpvu/dpvu:', ...
     matlabroot,'/toolbox/shared/dastudio/dpvu/dpvu/metamodel:', ...
     matlabroot,'/toolbox/shared/dastudio/dpvu/dpvu/actions:', ...
     matlabroot,'/toolbox/sldv/sldv:', ...
     matlabroot,'/toolbox/hdlcoder/hdlcommon:', ...
     matlabroot,'/toolbox/hdlcoder/hdlcommon/modelcheckeradvisor:', ...
     matlabroot,'/toolbox/shared/siglib:', ...
     matlabroot,'/toolbox/simulink/sta/derivedSignals:', ...
     matlabroot,'/toolbox/simulink/blocks/sb2sl:', ...
     matlabroot,'/toolbox/shared/codeinstrum/codeinstrum:', ...
     matlabroot,'/toolbox/idelink/foundation:', ...
     matlabroot,'/toolbox/idelink/foundation/util:', ...
     matlabroot,'/toolbox/idelink/foundation/errorhandler:', ...
     matlabroot,'/toolbox/idelink/foundation/xmakefile:', ...
     matlabroot,'/toolbox/idelink/foundation/hookpoints:', ...
     matlabroot,'/toolbox/shared/spcuilib:', ...
     matlabroot,'/toolbox/simulink/sta/scenarioconnector:', ...
     matlabroot,'/toolbox/simulink/sta/scenarioconnector/ui:', ...
     matlabroot,'/toolbox/simulink/sta/scenarioconnector/ui/toolstrip/modelsection:', ...
     matlabroot,'/toolbox/simulink/sta/scenarioconnector/ui/toolstrip/filesection:', ...
     matlabroot,'/toolbox/simulink/sta/editor/ui:', ...
     matlabroot,'/toolbox/simulink/sta/ui:', ...
     matlabroot,'/toolbox/simulink/sta/ui/comparisontool:', ...
     matlabroot,'/toolbox/simulink/sta/ui/mapping:', ...
     matlabroot,'/toolbox/simulink/sta/ui/mapping/callbacks:', ...
     matlabroot,'/toolbox/simulink/sta/ui/mapping/util:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip/open/streaming:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip/help:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip/open:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip/session:', ...
     matlabroot,'/toolbox/simulink/sta/ui/toolstrip/report:', ...
     matlabroot,'/toolbox/simulink/sltemplate:', ...
     matlabroot,'/toolbox/coder/coverage:', ...
     matlabroot,'/toolbox/shared/spcuilib/slscopes:', ...
     matlabroot,'/toolbox/shared/sl_messages:', ...
     matlabroot,'/toolbox/simulink/sta/sourceBlocks:', ...
     matlabroot,'/toolbox/shared/sigbldr:', ...
     matlabroot,'/toolbox/coder/rtiostream:', ...
     matlabroot,'/toolbox/fixedpoint/fixedpoint:', ...
     matlabroot,'/toolbox/shared/mldatx:', ...
     matlabroot,'/toolbox/shared/measure:', ...
     matlabroot,'/toolbox/simulink/sta/repository:', ...
     matlabroot,'/toolbox/simulink/sta/repository/util:', ...
     matlabroot,'/toolbox/fixedpoint/fixedpointtool:', ...
     matlabroot,'/toolbox/shared/hdlshared:', ...
     matlabroot,'/toolbox/shared/slvnv:', ...
     matlabroot,'/toolbox/shared/sdi:', ...
     matlabroot,'/toolbox/shared/sldv_sfcn/sldv_sfcn:', ...
     matlabroot,'/toolbox/target/codertarget:', ...
     matlabroot,'/toolbox/target/codertarget/rtw:', ...
     matlabroot,'/toolbox/target/codertarget/matlabcoder:', ...
     matlabroot,'/toolbox/shared/hwmanager/hwconnection:', ...
     matlabroot,'/toolbox/shared/sl_async_streaming:', ...
     matlabroot,'/toolbox/sl_pir_cap:', ...
     matlabroot,'/toolbox/shared/system/coreblocks:', ...
     matlabroot,'/toolbox/shared/configset:', ...
     matlabroot,'/toolbox/shared/sl_web_widgets:', ...
     matlabroot,'/toolbox/shared/system/simulink:', ...
     matlabroot,'/toolbox/coder/codedescriptor_core:', ...
     matlabroot,'/toolbox/simulink/fixedandfloat:', ...
     matlabroot,'/toolbox/simulink/fixedandfloat/obsolete:', ...
     matlabroot,'/toolbox/coder/profile:', ...
     matlabroot,'/toolbox/sl3d/sl3d:', ...
     matlabroot,'/toolbox/simulink/simulink/slproject/simulink:', ...
     matlabroot,'/toolbox/simulink/simulink/iodata/iomap:', ...
     matlabroot,'/toolbox/coder/connectivity:', ...
     matlabroot,'/toolbox/rtw/targets/connectivity:', ...
     matlabroot,'/toolbox/shared/dastudio/seqdiagram:', ...
     matlabroot,'/toolbox/shared/reqmgt:', ...
     matlabroot,'/toolbox/shared/reqmgt/mmutils:', ...
     matlabroot,'/toolbox/shared/eda/edagraph:', ...
     matlabroot,'/toolbox/simulink/simulink/iodata/ioformat:', ...
     matlabroot,'/toolbox/rtw/targets/AUTOSAR/AUTOSAR/dataclasses:', ...
     matlabroot,'/toolbox/rtw/targets/AUTOSAR/AUTOSAR:', ...
     matlabroot,'/toolbox/shared/simulation_data_repository:', ...
     matlabroot,'/toolbox/shared/sfcnlvl3/target:', ...
     matlabroot,'/toolbox/shared/sfcnlvl3/ddg:', ...
     matlabroot,'/toolbox/shared/sldv:', ...
     matlabroot,'/toolbox/rtw/rtw:', ...
     matlabroot,'/toolbox/coder/foundation:', ...
     matlabroot,'/toolbox/coder/foundation/build:', ...
     matlabroot,'/toolbox/coder/foundation/build/tools/registry:', ...
     matlabroot,'/toolbox/coder/foundation/tfl:', ...
     matlabroot,'/toolbox/coder/foundation/tfl/AUTOSAR/AUTOSAR4p0/IFX:', ...
     matlabroot,'/toolbox/coder/foundation/tfl/AUTOSAR/AUTOSAR4p0/IFL:', ...
     matlabroot,'/toolbox/coder/foundation/tfl/gui:', ...
     matlabroot,'/toolbox/coder/foundation/templates:', ...
     matlabroot,'/toolbox/shared/simtargets:', ...
     matlabroot,'/toolbox/shared/system/coder:', ...
     matlabroot,'/toolbox/coder/emlcodermex:', ...
     matlabroot,'/toolbox/shared/polyspace:', ...
     matlabroot,'/toolbox/coder/trace:', ...
     matlabroot,'/toolbox/simulink/simulink_data_dictionary/sldd:', ...
     matlabroot,'/toolbox/shared/slreportgen/reportexplorer:', ...
     matlabroot,'/toolbox/shared/slpir:', ...
     matlabroot,'/toolbox/shared/simulink/sysarch/sysarch:', ...
     matlabroot,'/toolbox/shared/eda/fil:', ...
     matlabroot,'/toolbox/shared/eda/fil/filmapi:', ...
     matlabroot,'/toolbox/simulink/slhistory:', ...
     matlabroot,'/toolbox/realtime:', ...
     matlabroot,'/toolbox/realtime/realtime:', ...
     matlabroot,'/toolbox/realtime/realtime/rtw:', ...
     matlabroot,'/toolbox/shared/cxxfe_mi/cxxfe_mi:', ...
     matlabroot,'/toolbox/simulink/simulink/performance:', ...
     matlabroot,'/toolbox/simulink/simulink/performance/performancea:', ...
     matlabroot,'/toolbox/simulink/compiled_model_interface:', ...
     matlabroot,'/toolbox/shared/sl_coverage_configset:', ...
     matlabroot,'/toolbox/shared/eda/board:', ...
     matlabroot,'/toolbox/shared/hdlshared/hdlshared_gui:', ...
     matlabroot,'/toolbox/sl3d/sl3ddemos:', ...
     matlabroot,'/toolbox/shared/eda/fpgabase:', ...
     matlabroot,'/toolbox/slvnv/simcoverage:', ...
     matlabroot,'/toolbox/shared/cgir_fe:', ...
     matlabroot,'/toolbox/coder/coder:', ...
     matlabroot,'/toolbox/shared/maputils:', ...
     matlabroot,'/toolbox/shared/eda/fpgaautomation:', ...
     matlabroot,'/toolbox/shared/eda/fpgaautomation/obsolete:', ...
     matlabroot,'/toolbox/simulink/simulink/slproject/examples:', ...
     matlabroot,'/toolbox/simulink/simulink/slproject/templates:', ...
     matlabroot,'/toolbox/simulink/sl_async_streaming:', ...
     matlabroot,'/toolbox/shared/eda/hdlparser:', ...
     matlabroot,'/toolbox/hdlcoder/hdllib/sl_lib:', ...
     matlabroot,'/toolbox/shared/slci/slci:', ...
     matlabroot,'/toolbox/eml/eml:', ...
     matlabroot,'/toolbox/shared/simulink/slcheck_services:', ...
     matlabroot,'/toolbox/shared/dsp/dialog:', ...
     matlabroot,'/toolbox/fixedpoint/fidemos:', ...
     matlabroot,'/toolbox/hdlcoder/hdlslrt:', ...
     matlabroot,'/toolbox/simulink/slexportprevious:', ...
     matlabroot,'/toolbox/bioinfo/bioinfodata:', ...
     matlabroot,'/toolbox/comm/cdma2000:', ...
     matlabroot,'/toolbox/comm/commdemos:', ...
     matlabroot,'/help/toolbox/comm/examples:', ...
     matlabroot,'/toolbox/comm/comm:', ...
     matlabroot,'/toolbox/comm/commutilities/comminit:', ...
     matlabroot,'/toolbox/comm/commutilities/commmex:', ...
     matlabroot,'/toolbox/comm/commutilities:', ...
     matlabroot,'/toolbox/comm/commdeprecated:', ...
     matlabroot,'/toolbox/comm/comm/compiled:', ...
     matlabroot,'/toolbox/comm/templates:', ...
     matlabroot,'/toolbox/shared/testconsole:', ...
     matlabroot,'/toolbox/shared/optimlib:', ...
     matlabroot,'/toolbox/nnet/cnn:', ...
     matlabroot,'/toolbox/shared/dsp/vision/matlab/utilities:', ...
     matlabroot,'/toolbox/shared/dsp/vision/simulink/utilities:', ...
     matlabroot,'/toolbox/shared/dsp/vision/matlab/utilities/mex:', ...
     matlabroot,'/toolbox/shared/dsp/vision/simulink/utilities/mex:', ...
     matlabroot,'/toolbox/shared/dsp/vision/matlab/utilities/init:', ...
     matlabroot,'/toolbox/shared/dsp/vision/matlab/vision:', ...
     matlabroot,'/toolbox/shared/dsp/vision/simulink/vision:', ...
     matlabroot,'/toolbox/shared/slcontrollib:', ...
     matlabroot,'/toolbox/control/control:', ...
     matlabroot,'/toolbox/control/ctrlmodels:', ...
     matlabroot,'/toolbox/control/ctrlanalysis:', ...
     matlabroot,'/toolbox/control/ctrldesign:', ...
     matlabroot,'/toolbox/control/ctrlplots:', ...
     matlabroot,'/toolbox/control/ctrlguis:', ...
     matlabroot,'/toolbox/control/ctrlobsolete:', ...
     matlabroot,'/toolbox/control/ctrlutil:', ...
     matlabroot,'/toolbox/shared/controllib/requirements:', ...
     matlabroot,'/toolbox/control/ctrldemos:', ...
     matlabroot,'/toolbox/shared/tracking/trackinglib:', ...
     matlabroot,'/help/toolbox/control/examples:', ...
     matlabroot,'/toolbox/shared/controllib/engine:', ...
     matlabroot,'/toolbox/shared/controllib/engine/numerics:', ...
     matlabroot,'/toolbox/shared/controllib/engine/options:', ...
     matlabroot,'/toolbox/shared/controllib/engine/optim:', ...
     matlabroot,'/toolbox/shared/controllib/engine/blocks:', ...
     matlabroot,'/toolbox/curvefit/curvefit:', ...
     matlabroot,'/toolbox/curvefit/splines:', ...
     matlabroot,'/toolbox/curvefit/sftoolgui:', ...
     matlabroot,'/toolbox/curvefit/curvefitdemos:', ...
     matlabroot,'/toolbox/shared/curvefitlib:', ...
     matlabroot,'/toolbox/dsp/dspdeployabledemos:', ...
     matlabroot,'/toolbox/shared/dsp/visionhdl/simulink/dsp:', ...
     matlabroot,'/toolbox/dsp/templates:', ...
     matlabroot,'/toolbox/dsp/dspdemos:', ...
     matlabroot,'/toolbox/dsp/dsp:', ...
     matlabroot,'/toolbox/dsp/dsputilities:', ...
     matlabroot,'/toolbox/dsp/dsputilities/dspinit:', ...
     matlabroot,'/toolbox/dsp/dsputilities/dspmex:', ...
     matlabroot,'/toolbox/dsp/dsp/compiled:', ...
     matlabroot,'/toolbox/dsp/filterdesign:', ...
     matlabroot,'/toolbox/shared/dsp/hdl:', ...
     matlabroot,'/toolbox/shared/dsp/scopes:', ...
     matlabroot,'/help/toolbox/dsp/examples:', ...
     matlabroot,'/toolbox/fuzzy/fuzdemos:', ...
     matlabroot,'/toolbox/fuzzy/fuzzy:', ...
     matlabroot,'/toolbox/fuzzy/fuzzyutil:', ...
     matlabroot,'/toolbox/images/imdemos:', ...
     matlabroot,'/toolbox/images/colorspaces:', ...
     matlabroot,'/toolbox/images/images:', ...
     matlabroot,'/toolbox/images/imdata:', ...
     matlabroot,'/toolbox/images/imuitools:', ...
     matlabroot,'/toolbox/images/iptformats:', ...
     matlabroot,'/toolbox/images/iptutils:', ...
     matlabroot,'/toolbox/shared/imageslib:', ...
     matlabroot,'/toolbox/instrument/instrument:', ...
     matlabroot,'/toolbox/instrument/instrumentblks/instrumentblks:', ...
     matlabroot,'/toolbox/instrument/instrumentblks/instrumentmex:', ...
     matlabroot,'/toolbox/instrument/instrumentblks/instrumentmasks:', ...
     matlabroot,'/toolbox/instrument/instrumentdemos:', ...
     matlabroot,'/toolbox/shared/testmeaslib/simulink:', ...
     matlabroot,'/toolbox/shared/mapgeodesy:', ...
     matlabroot,'/toolbox/map/map:', ...
     matlabroot,'/toolbox/map/mapgeodesy:', ...
     matlabroot,'/toolbox/map/mapdisp:', ...
     matlabroot,'/toolbox/map/mapformats:', ...
     matlabroot,'/toolbox/map/mapproj:', ...
     matlabroot,'/toolbox/map/mapdata:', ...
     matlabroot,'/toolbox/map/mapdata/sdts:', ...
     matlabroot,'/toolbox/geoweb/geoweb:', ...
     matlabroot,'/toolbox/compiler/java:', ...
     matlabroot,'/toolbox/javabuilder/javabuilder:', ...
     matlabroot,'/toolbox/compiler/mltall:', ...
     matlabroot,'/toolbox/compiler/mlspark:', ...
     matlabroot,'/toolbox/compiler:', ...
     matlabroot,'/toolbox/shared/bigdata:', ...
     matlabroot,'/toolbox/compiler/compilerdemos:', ...
     matlabroot,'/toolbox/compiler/mlhadoop:', ...
     matlabroot,'/toolbox/compiler_sdk/java:', ...
     matlabroot,'/toolbox/compiler_sdk:', ...
     matlabroot,'/toolbox/nnet:', ...
     matlabroot,'/toolbox/nnet/nncontrol:', ...
     matlabroot,'/toolbox/nnet/nnet:', ...
     matlabroot,'/toolbox/nnet/nnet/nnadapt:', ...
     matlabroot,'/toolbox/nnet/nnet/nndatafun:', ...
     matlabroot,'/toolbox/nnet/nnet/nnderivative:', ...
     matlabroot,'/toolbox/nnet/nnet/nndistance:', ...
     matlabroot,'/toolbox/nnet/nnet/nndivision:', ...
     matlabroot,'/toolbox/nnet/nnet/nninitlayer:', ...
     matlabroot,'/toolbox/nnet/nnet/nninitnetwork:', ...
     matlabroot,'/toolbox/nnet/nnet/nninitweight:', ...
     matlabroot,'/toolbox/nnet/nnet/nnlearn:', ...
     matlabroot,'/toolbox/nnet/nnet/nnnetfun:', ...
     matlabroot,'/toolbox/nnet/nnet/nnnetinput:', ...
     matlabroot,'/toolbox/nnet/nnet/nnnetwork:', ...
     matlabroot,'/toolbox/nnet/nnet/nnperformance:', ...
     matlabroot,'/toolbox/nnet/nnet/nnplot:', ...
     matlabroot,'/toolbox/nnet/nnet/nnprocess:', ...
     matlabroot,'/toolbox/nnet/nnet/nnsearch:', ...
     matlabroot,'/toolbox/nnet/nnet/nntopology:', ...
     matlabroot,'/toolbox/nnet/nnet/nntrain:', ...
     matlabroot,'/toolbox/nnet/nnet/nntransfer:', ...
     matlabroot,'/toolbox/nnet/nnet/nnweight:', ...
     matlabroot,'/toolbox/nnet/nnguis:', ...
     matlabroot,'/toolbox/nnet/nnobsolete:', ...
     matlabroot,'/toolbox/nnet/nnutils:', ...
     matlabroot,'/toolbox/nnet/nndemos:', ...
     matlabroot,'/toolbox/nnet/nndemos/nndatasets:', ...
     matlabroot,'/toolbox/optim/optimdemos:', ...
     matlabroot,'/toolbox/optim/optim:', ...
     matlabroot,'/toolbox/optim:', ...
     matlabroot,'/toolbox/distcomp:', ...
     matlabroot,'/toolbox/distcomp/distcomp:', ...
     matlabroot,'/toolbox/distcomp/user:', ...
     matlabroot,'/toolbox/distcomp/mpi:', ...
     matlabroot,'/toolbox/distcomp/parallel:', ...
     matlabroot,'/toolbox/distcomp/parallel/util:', ...
     matlabroot,'/toolbox/distcomp/lang:', ...
     matlabroot,'/toolbox/distcomp/cluster:', ...
     matlabroot,'/toolbox/distcomp/gpu:', ...
     matlabroot,'/toolbox/distcomp/array:', ...
     matlabroot,'/toolbox/distcomp/bigdata:', ...
     matlabroot,'/toolbox/pde:', ...
     matlabroot,'/toolbox/pde/pdedata:', ...
     matlabroot,'/toolbox/distcomp/pctdemos:', ...
     matlabroot,'/toolbox/shared/pdelib:', ...
     matlabroot,'/toolbox/pde/pdedemos:', ...
     matlabroot,'/toolbox/signal/sigdemos:', ...
     matlabroot,'/toolbox/shared/filterdesignlib:', ...
     matlabroot,'/toolbox/shared/filterdesignlib/filterbuilder:', ...
     matlabroot,'/toolbox/signal/signalanalyzer:', ...
     matlabroot,'/toolbox/signal/signal:', ...
     matlabroot,'/toolbox/signal/sigtools:', ...
     matlabroot,'/toolbox/signal/sptoolgui:', ...
     matlabroot,'/toolbox/physmod/simscape/compiler/mli/m:', ...
     matlabroot,'/toolbox/physmod/common/gl/sli/m:', ...
     matlabroot,'/toolbox/physmod/common/foundation/mli/m:', ...
     matlabroot,'/toolbox/physmod/network_engine/network_engine:', ...
     matlabroot,'/toolbox/physmod/simscape/simscapedemos:', ...
     matlabroot,'/toolbox/physmod/simscape/foundation/simscape:', ...
     matlabroot,'/toolbox/physmod/simscape/engine/core/m:', ...
     matlabroot,'/toolbox/physmod/simscape/library/m:', ...
     matlabroot,'/toolbox/physmod/common/units/mli/m:', ...
     matlabroot,'/toolbox/physmod/simscape/engine/sli/m:', ...
     matlabroot,'/toolbox/physmod/simscape/templates:', ...
     matlabroot,'/toolbox/physmod/simscape/simscape/m:', ...
     matlabroot,'/toolbox/physmod/common/external/library/m:', ...
     matlabroot,'/toolbox/physmod/pm_sli/pm_sli:', ...
     matlabroot,'/toolbox/physmod/common/external/mli/m:', ...
     matlabroot,'/toolbox/physmod/common/dataservices/mli/m:', ...
     matlabroot,'/toolbox/physmod/simscape/advisor/m:', ...
     matlabroot,'/toolbox/physmod/common/dataservices/gui/m:', ...
     matlabroot,'/toolbox/physmod/common/data/mli/m:', ...
     matlabroot,'/toolbox/physmod/simscape/compiler/sli/m:', ...
     matlabroot,'/toolbox/physmod/common/dataservices/core/m:', ...
     matlabroot,'/toolbox/physmod/simscape/engine/mli/m:', ...
     matlabroot,'/toolbox/physmod/ne_sli/ne_sli:', ...
     matlabroot,'/toolbox/physmod/common/gl/mli/m:', ...
     matlabroot,'/toolbox/physmod/common/dataservices/sli/m:', ...
     matlabroot,'/toolbox/slcontrol/slcontrol:', ...
     matlabroot,'/toolbox/slcontrol/slctrlguis:', ...
     matlabroot,'/toolbox/slcontrol/slctrlutil:', ...
     matlabroot,'/toolbox/slcontrol/slctrlobsolete:', ...
     matlabroot,'/toolbox/physmod/sm/import/m:', ...
     matlabroot,'/toolbox/physmod/sm/docexamples:', ...
     matlabroot,'/toolbox/physmod/sm/ssci/m:', ...
     matlabroot,'/toolbox/physmod/sm/core/m:', ...
     matlabroot,'/toolbox/physmod/mech/mechdemos:', ...
     matlabroot,'/toolbox/physmod/sm/sm/m:', ...
     matlabroot,'/toolbox/physmod/gui/gfx/m:', ...
     matlabroot,'/toolbox/physmod/sm/sli/m:', ...
     matlabroot,'/toolbox/physmod/pm_visimpl/pm_visimpl:', ...
     matlabroot,'/toolbox/physmod/sm/gui/m:', ...
     matlabroot,'/toolbox/physmod/sm/local/m:', ...
     matlabroot,'/toolbox/physmod/sm/foundation/mech:', ...
     matlabroot,'/toolbox/physmod/sm/templates:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/wing_landing_gear:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/wing_landing_gear/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/cart_double_pendulum:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/cart_double_pendulum/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/stewart_platform:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/stewart_platform/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/double_crank_aiming:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/double_crank_aiming/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/bread_slicer:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/bread_slicer/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/import/stewart_platform:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/import/four_bar:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/radial_engine:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/radial_engine/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/double_wishbone_suspension:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/double_wishbone_suspension/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/import/robot:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/cardan_gear:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/cardan_gear/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/windshield_wiper:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/windshield_wiper/images:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/backhoe:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/robotic_wrist:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/carousel:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/welding_robot:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/import/robot_stepfiles:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/potters_wheel:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/linear_actuator:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/pto_shaft:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/cam_flapping_wing:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/vehicle_slalom:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/dump_trailer:', ...
     matlabroot,'/toolbox/physmod/sm/smdemos/interface_elements:', ...
     matlabroot,'/toolbox/physmod/mech/mech:', ...
     matlabroot,'/toolbox/physmod/mech/importer:', ...
     matlabroot,'/toolbox/slcontrol/slctrldemos:', ...
     matlabroot,'/help/toolbox/slcontrol/examples:', ...
     matlabroot,'/toolbox/stats/stats:', ...
     matlabroot,'/toolbox/stats/classreg:', ...
     matlabroot,'/toolbox/stats/clustering:', ...
     matlabroot,'/toolbox/stats/bayesoptim:', ...
     matlabroot,'/toolbox/stats/bigdata:', ...
     matlabroot,'/toolbox/stats/mlearnapp:', ...
     matlabroot,'/toolbox/shared/statslib:', ...
     matlabroot,'/toolbox/shared/statslib/sensitivity:', ...
     matlabroot,'/toolbox/stats/statsdemos:', ...
     matlabroot,'/toolbox/stats/gpu:', ...
     matlabroot,'/toolbox/symbolic/symbolic:', ...
     matlabroot,'/toolbox/symbolic/symbolicdemos:', ...
     matlabroot,'/toolbox/ident/iddemos:', ...
     matlabroot,'/toolbox/ident/iddemos/examples:', ...
     matlabroot,'/toolbox/ident/ident:', ...
     matlabroot,'/toolbox/ident/nlident:', ...
     matlabroot,'/toolbox/ident/idobsolete:', ...
     matlabroot,'/toolbox/ident/idguis:', ...
     matlabroot,'/toolbox/ident/idutils:', ...
     matlabroot,'/toolbox/ident/idrecursive:', ...
     matlabroot,'/toolbox/ident/idhelp:', ...
     matlabroot,'/toolbox/wavelet/wavelet:', ...
     matlabroot,'/toolbox/wavelet/wmultisig1d:', ...
     matlabroot,'/toolbox/wavelet/compression:', ...
     matlabroot,'/toolbox/wavelet/wavedemo:', ...
     matlabroot,'/toolbox/bioinfo/biodemos:', ...
     matlabroot,'/toolbox/bioinfo/bioinfo:', ...
     matlabroot,'/toolbox/bioinfo/biolearning:', ...
     matlabroot,'/toolbox/bioinfo/microarray:', ...
     matlabroot,'/toolbox/bioinfo/mass_spec:', ...
     matlabroot,'/toolbox/bioinfo/proteins:', ...
     matlabroot,'/toolbox/bioinfo/biomatrices:', ...
     matlabroot,'/toolbox/bioinfo/graphtheory:', ...
     matlabroot,'/toolbox/vision/vision:', ...
     matlabroot,'/toolbox/vision/visiondata:', ...
     matlabroot,'/toolbox/vision/visionutilities:', ...
     matlabroot,'/toolbox/vision/visionutilities/visioninit:', ...
     matlabroot,'/toolbox/vision/visionutilities/visionmex:', ...
     matlabroot,'/help/toolbox/vision/examples:', ...
     matlabroot,'/toolbox/vision/visiondemos:', ...
%%% END ENTRIES %%%
     ...
];

if strncmp(computer, 'PC', 2) % ispc and pathsep not available
    separator = ';';
else
    separator = ':';
end

p = [userpath separator getenv('MATLABPATH') separator p];
