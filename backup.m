
clc
clear
close all

%% code modified from KSVD package available http://www.cs.technion.ac.il/�ronrubin/software.html.


disp('  **********  K-SVD MRI data **********');
% sparsity=[1 2  4 5 8  10 15 20 50 100 200 500 1000];
% dictionary_size=[2 4 5 8 10 15 20 50 100 200 500 1000];
% for iter=1:size(dictionary_size,2)
clear Dksvd g S_star
% dictionary dimensions
m = 499; %K
% sparsity of each example
k = 2; %T

load ('/extra/krishna/ksvd/mri_2012_HG.mat'); % loading datasets from previous homework

training_data=mri_collection{1,1};
% test_data=mri_collection{:,16:20};
clear mri_collection;
load ('/extra/krishna/ksvd/mri_gt.mat');
ground_truth=mri_collection{1,1};
clear mri_collection;


voxel_dim=5;
voxel_size=voxel_dim*voxel_dim*voxel_dim;
v=floor(voxel_dim/2);
% for set=1:15


[A,C,S]=size(training_data);
for s=1:S
mri_data{s,1}=sparse(double(training_data(:,:,s)));
end
clear training_data
labels=ground_truth{1,1};
% mri_data=mri_data/max(max(max(mri_data)));
[A,C,S]=size(mri_data);%axial coronal sagittal
voxel=cell(A-2*v,C-2*v,S-2*v);
i=1;
% voxel_vec=zeros(voxel_size,(A-2*v)*(C-2*v)*(S-2*v));
% voxel_label=zeros(1,(A-2*v)*(C-2*v)*(S-2*v));
for a=v+1:A-v
    for c=v+1:C-v
        for s=v+1:S-v
%             voxel{a-v,c-v,s-v}=mri_data(a-v:a+v,c-v:c+v,s-v:s+v);
%             voxel_vec(:,i)=reshape(voxel{a-v,c-v,s-v},voxel_size,1)/max(voxel{a-v,c-v,s-v});
%               voxel_vec(:,i)=reshape(mri_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1)/max(max(max(mri_data(a-v:a+v,c-v:c+v,s-v:s+v))));
            i=i+1;
            voxel_label(a-v,c-v,s-v)=ground_truth{1,1}(a,c,s);
            
        end
    end
end

% 
% % end
% 
% 
% for dig=1:10
%     temp=images(:,labels==dig-1);
%     T(:,:,dig)=temp(:,1:5000)';
% end
% %%
% M=500;
% N=250;
% Train_set=T(1:M,:,1:10);
% vald_set=T(M+1:M+N,:,1:10);
% 
% %% run k-svd training %%
% for dig=1:10
%     dig
%     params.data = Train_set(:,:,dig)';
%     params.Tdata = k;
%     params.testdata = vald_set(:,:,dig)';
%     params.dictsize = m;
%     params.iternum = 10;
%     params.memusage = 'high';
%     
%     [Dksvd(:,:,dig),g{1,dig},err(:,:)] = ksvd(params,''); % used the package funtion mentioned above
%     
% end
% 
% %% testing
% 
% Test_set=T(751:1000,:,1:10);
% 
% for test_dig=1:10
%     test_dig
%     for i=1:250
%         for dig=1:10 %for each digit
%             S_star(:,dig)=OMP(Dksvd(:,:,dig),Test_set(i,:,test_dig)',k); %applying OMP to find S
%             %OMP function code attached seperately
%             Z(:,dig)=Test_set(i,:,test_dig)'-Dksvd(:,:,dig)*S_star(:,dig); % finding the value of ||X-DS||
%         end
%         [min_val,result(i,test_dig)]=min(sum(Z.^2)); %finding min of ||X-DS||^2
%         %      min_arg
%     end
% end
% %% Results
% for dig=1:10
%     accuracy(1,dig)=sum(result(:,dig)==dig);
% end
% accuracy_per=accuracy./250;
% 
% string=sprintf('k%dm%d.mat',m,k);
% save(string);
% % end