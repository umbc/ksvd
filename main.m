
clc
clear
close all

% code modified from KSVD package available http://www.cs.technion.ac.il/�ronrubin/software.html.


disp('  **********  K-SVD MRI data **********');
sparsity=[1 2  4 5 8  10 15 20 50 100 200 500 1000];
dictionary_size=[2 4 5 8 10 15 20 50 100 200 500 1000];
for iter=1:size(dictionary_size,2)
clear Dksvd g S_star
dictionary dimensions
m = 499; %K
% sparsity of each example
k = 2; %T

load ('/extra/krishna/ksvd/mri_2012_HG.mat'); % loading datasets from previous homework

training_data=double(mri_collection{1,1});

load ('/extra/krishna/ksvd/mri_gt.mat');
ground_truth=mri_collection{1,1};
clear mri_collection;


voxel_dim=5;
voxel_size=voxel_dim*voxel_dim*voxel_dim;
v=floor(voxel_dim/2);
% for set=1:15


% [A,C,S]=size(training_data);
% for s=1:S
% mri_data{s,1}=sparse(double(training_data(:,:,s)));
% end
% clear training_data

% mri_data=mri_data/max(max(max(mri_data)));
[A,C,S]=size(training_data);%axial coronal sagittal
% voxel=cell(A-2*v,C-2*v,S-2*v);
i0=1;i1=1;i2=1;i3=1;i4=1;
% voxel_vec=zeros(voxel_size,(A-2*v)*(C-2*v)*(S-2*v));
%   voxel_label=ground_truth(v+1:A-v,v+1:C-v,v+1:S-v);
for a=v+1:A-v
    for c=v+1:C-v
        for s=v+1:S-v
%             if(ground_truth(a,c,s)~=0)
                %             voxel0(:,i0)=sparse(reshape(training_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
                %             i0=i0+1;

                    if(ground_truth(a,c,s)==1)
                    voxel1(:,i1)=sparse(reshape(training_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
                    i1=i1+1;
                    elseif(ground_truth(a,c,s)==2)
                        voxel2(:,i2)=sparse(reshape(training_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
                        i2=i2+1;
                    elseif(ground_truth(a,c,s)==3)
                        voxel3(:,i3)=sparse(reshape(training_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
                        i3=i3+1;
                    elseif(ground_truth(a,c,s)==4)
                        voxel4(:,i4)=sparse(reshape(training_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
                        i4=i4+1;
                    end

%             end
        end
    end
    a
end


clear ground_truth training_data
load ('/extra/krishna/ksvd/mri_2012_HG.mat'); % loading datasets from previous homework
test_data=double(mri_collection{1,2});
clear mri_collection;

it=1;
for a=80:100
    for c=80:100
        for s=80:100

            voxelt(:,it)=sparse(reshape(test_data(a-v:a+v,c-v:c+v,s-v:s+v),voxel_size,1));
            it=it+1;

        end
    end
    a
end


%% 
% load train_data_1
% load vald_data
%


%% run k-svd training %%
for dig=1:4
    dig
%     if dig==0
%         params.data = voxel0;
    if dig==1
        params.data = voxel1;
    elseif dig==2
        params.data = voxel2;
    elseif dig==3
        params.data = voxel3;
    elseif dig==4
        params.data = voxel4;
    end
    params.Tdata = k;
    params.testdata = voxelt;
    params.dictsize = m;
    params.iternum = 10;
    params.memusage = 'high';

     if dig==1
        [Dksvd1,g{1,dig},err(:,:)] = ksvd(params,'');
    elseif dig==2
        [Dksvd2,g{1,dig},err(:,:)] = ksvd(params,'');
    elseif dig==3
        [Dksvd3,g{1,dig},err(:,:)] = ksvd(params,'');
    elseif dig==4
        [Dksvd4,g{1,dig},err(:,:)] = ksvd(params,'');
    end
     % used the package funtion mentioned above

end

% save dictionary  Dksvd1 Dksvd2 Dksvd3 Dksvd4
% testing

% load dictionary
% load test_data

Test_set=voxelt;
[~,no_test_vox]=size(voxelt);
% 

    for i=1:no_test_vox
        for dig=1:4 %for each digit
            
            if dig==1
              S_star1=OMP(Dksvd1,Test_set(:,i),k);
               Z(:,dig)=Test_set(:,i)-Dksvd1*S_star1;
            elseif dig==2
                 S_star1=OMP(Dksvd1,Test_set(:,i),k);
               Z(:,dig)=Test_set(:,i)-Dksvd1*S_star1;
            elseif dig==3
                 S_star1=OMP(Dksvd1,Test_set(:,i),k);
               Z(:,dig)=Test_set(:,i)-Dksvd1*S_star1;
            elseif dig==4
                 S_star1=OMP(Dksvd1,Test_set(:,i),k);
               Z(:,dig)=Test_set(:,i)-Dksvd1*S_star1;
            end
             %applying OMP to find S
            %OMP function code attached seperately
            % finding the value of ||X-DS||
        end
        [min_val,result(i,1)]=min(sum(Z.^2)); %finding min of ||X-DS||^2
        %      min_arg
    end

% Results

% load mri_gt.mat;
% test_gt=mri_collection{1,2};


%     accuracy(1,dig)=sum(result==reshape(test_gt(80:100,80:100,80:100),9261,1));

% accuracy_per=accuracy./250;

% string=sprintf('k%dm%d.mat',m,k);
% save(string);
